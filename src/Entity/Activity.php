<?php

namespace Drupal\contacts_activity\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\UserInterface;

/**
 * Defines the Activity entity.
 *
 * @ingroup contacts_activity
 *
 * @ContentEntityType(
 *   id = "c_activity",
 *   label = @Translation("Activity"),
 *   bundle_label = @Translation("Type"),
 *   label_singular = @Translation("Activity"),
 *   label_plural = @Translation("Activities"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Activity",
 *     plural = "@count Activities"
 *   ),
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\contacts_activity\Form\ActivityForm",
 *       "add" = "Drupal\contacts_activity\Form\ActivityForm",
 *       "edit" = "Drupal\contacts_activity\Form\ActivityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "views_data" = "Drupal\contacts_activity\ActivityViewsData",
 *     "route_provider" = {
 *       "html" = "Drupal\contacts_activity\ActivityHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "c_activity",
 *   data_table = "c_activity",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "bundle" = "type",
 *   },
 *   admin_permission = "administer activity entities",
 *   links = {
 *     "add-form" = "/admin/contacts/{user}/c_activity/{c_activity_type}",
 *     "edit-form" = "/admin/contacts/c_activity/{c_activity}/edit",
 *     "delete-form" = "/admin/contacts/c_activity/{c_activity}/delete",
 *   },
 *   bundle_entity_type = "c_activity_type",
 *   field_ui_base_route = "entity.c_activity_type.edit_form",
 * )
 */
class Activity extends ContentEntityBase implements ActivityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Creator'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', ['region' => 'hidden'])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['participants'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Participants'))
      ->setDescription(t('Those involved in the activity.'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setRequired(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'search_api')
      ->setSetting('handler_settings', [
        'index' => 'contacts_index',
        'conditions' => [['roles', 'crm_indiv']],
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['related_to'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Related to'))
      ->setDescription(t('The company to whom the activity is related.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'search_api')
      ->setSetting('handler_settings', [
        'index' => 'contacts_index',
        'conditions' => [['roles', 'crm_org']],
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['related_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Related To Type'))
      ->setSetting('allowed_values_function', [
        '\Drupal\contacts_activity\Entity\Activity',
        'getTypes',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Status'))
      ->setRequired(TRUE)
      ->setSetting('allowed_values_function', [
        '\Drupal\contacts_activity\Entity\Activity',
        'getStatuses',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status_log'] = BaseFieldDefinition::create('status_log')
      ->setLabel(t('Status History'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('A log of when the status was changed and by whom.'))
      ->setSettings([
        'source_field' => 'status',
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The name of the Subscription entity.'))
      ->setSettings([
        'max_length' => 254,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['date'] = BaseFieldDefinition::create('daterange')
      ->setLabel(new TranslatableMarkup('Date'))
      ->setDescription(new TranslatableMarkup('Date and time of the activity.'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * Gets all related to type options.
   */
  public static function getTypes(FieldStorageDefinitionInterface $definition, ContentEntityInterface $entity = NULL, $cacheable = FALSE) : array {
    // @fixme Can we cache this?
    $cacheable = FALSE;

    $types = [
      'accounts' => new TranslatableMarkup('Accounts'),
      'cases' => new TranslatableMarkup('Cases'),
      'contacts' => new TranslatableMarkup('Contacts'),
      'opportunities' => new TranslatableMarkup('Opportunities'),
      'tasks' => new TranslatableMarkup('Tasks'),
    ];

    // Allow other modules to add/alter list of possible activity related types.
    \Drupal::moduleHandler()->alter('contacts_activity_get_types', $types, $entity);

    return $types;
  }

  /**
   * Gets all status options.
   */
  public static function getStatuses(FieldStorageDefinitionInterface $definition, ContentEntityInterface $entity = NULL, $cacheable = FALSE) : array {
    // @fixme Can we cache this?
    $cacheable = FALSE;

    $types = [
      'not_started' => new TranslatableMarkup('Not Started'),
      'planned' => new TranslatableMarkup('Planned'),
      'held' => new TranslatableMarkup('Held'),
      'not_held' => new TranslatableMarkup('Not Held'),
      'in_progress' => new TranslatableMarkup('In Progress'),
      'deferred' => new TranslatableMarkup('Deferred'),
      'completed' => new TranslatableMarkup('Completed'),
    ];

    // Allow other modules to add/alter list of possible Geocoding Field Types.
    \Drupal::moduleHandler()->alter('contacts_activity_get_statuses', $types, $entity);

    return $types;
  }

}
