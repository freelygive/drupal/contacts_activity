<?php

namespace Drupal\contacts_activity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Activity Type
 *
 * @ConfigEntityType(
 *   id = "c_activity_type",
 *   label = @Translation("Activity Type"),
 *   label_singular = @Translation("Activity Type"),
 *   label_collection = @Translation("Activity Types"),
 *   label_plural = @Translation("Activity Types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count activity type",
 *     plural = "@count activities types"
 *   ),
 *   bundle_of = "c_activity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_prefix = "c_activity_type",
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\contacts_activity\Form\ActivityTypeForm",
 *       "add" = "Drupal\contacts_activity\Form\ActivityTypeForm",
 *       "edit" = "Drupal\contacts_activity\Form\ActivityTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\contacts_activity\ActivityTypeAccessControlHandler",
 *     "list_builder" = "Drupal\contacts_activity\ActivityTypeListBuilder",
 *   },
 *   admin_permission = "administer site configuration",
 *   links = {
 *     "canonical" = "/admin/structure/activity_type/{c_activity_type}",
 *     "add-form" = "/admin/structure/activity_type/add",
 *     "edit-form" = "/admin/structure/activity_type/{c_activity_type}/edit",
 *     "delete-form" = "/admin/structure/activity_type/{c_activity_type}/delete",
 *     "collection" = "/admin/structure/activity_type",
 *   }
 * )
 */
class ActivityType extends ConfigEntityBundleBase {}
