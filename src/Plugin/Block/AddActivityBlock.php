<?php

namespace Drupal\contacts_activity\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an add activity block for the contacts tab.
 *
 * @Block(
 *   id = "contacts_activity_add_activity_modal",
 *   admin_label = @Translation("Add activity modal links"),
 *   category = @Translation("Contacts"),
 *   context_definitions = {
 *     "user" = @ContextDefinition("entity:user", required = TRUE, label = @Translation("User"))
 *   }
 * )
 */
class AddActivityBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The DBS workforce storage handler.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $activityTypeStorage;

  /**
   * Constructs a new StatusArchiveWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->activityTypeStorage = $entity_type_manager->getStorage('c_activity_type');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    if (!empty($this->configuration['label'])) {
      return $this->configuration['label'];
    }

    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $block = [];

    $user = $this->getContextValue('user');
    $types = $this->activityTypeStorage->loadMultiple();
    foreach ($types as $type) {
      $block[$type->id()] = [
        '#type' => 'link',
        '#title' => $this->t('+ Add %type', [
          '%type' => $type->label(),
        ]),
        '#url' => Url::fromRoute('entity.c_activity.add_form', [
          'user' => $user->id(),
          'c_activity_type' => $type->id(),
        ], [
          'query' => \Drupal::destination()->getAsArray(),
        ]),
        '#attributes' => ['class' => ['clearfix']],
      ];
    }

    return $block;
  }

}
