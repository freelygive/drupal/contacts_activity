<?php

namespace Drupal\contacts_activity;

use Drupal\views\EntityViewsData;

/**
 * Provides the Views data for the contacts activity entity type.
 */
class ActivityViewsData extends EntityViewsData {

}
