<?php

namespace Drupal\contacts_activity\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for the activity type entity edit forms.
 */
class ActivityTypeForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $entity = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('The name of the new activity type.'),
      '#required' => TRUE,
      '#default_value' => $entity->label(),
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => '\Drupal\contacts_activity\Entity\ActivityType::load',
        'source' => ['label'],
        'replace_pattern' => '[^a-z0-9_]+',
        'replace' => '_',
      ],
      '#default_value' => $entity->id(),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $this->entity->save();
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
  }
}
