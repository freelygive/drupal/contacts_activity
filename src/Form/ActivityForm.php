<?php

namespace Drupal\contacts_activity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for the activity forms.
 */
class ActivityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $user = NULL) {
    // Set the entity owner if provided by url.
    if (!empty($user)) {
      /** @var \Drupal\contacts_activity\Entity\Activity $entity */
      $entity = $this->getEntity();
      /** @var \Drupal\Core\Session\AccountInterface $user */
      $user = $this->entityTypeManager->getStorage('user')->load($user);

      // Set individual defaults.
      if (in_array('crm_indiv', $user->getRoles())) {
        $entity->set('participants', $user);

        if ($entity->hasField('activity_assigned_to')) {
          $entity->set('activity_assigned_to', $user);
        }
      }

      // Set organisation defaults.
      if (in_array('crm_org', $user->getRoles())) {
        $entity->set('related_to', $user);
      }
    }

    return parent::buildForm($form, $form_state);
  }

}
